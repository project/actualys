; Drupal.org release file.
core = 7.x
api = 2

defaults[projects][subdir] = contrib

; Basic contributed modules.
projects[ctools][version] = 1.9
projects[date][version] = 2.9
projects[diff][version] = 3.2
projects[email][version] = 1.3
projects[entity][version] = 1.6
projects[entityreference][version] = 1.1
projects[entityreference][patch][] = "http://drupal.org/files/1580348-universal-formatters-17.patch"
projects[features][version] = 2.7
projects[features][patch][2143765] = "http://drupal.org/files/issues/features-fix-modules-enabled-2143765-1.patch"
projects[features][patch][2479803] = "https://www.drupal.org/files/issues/ignore_hidden_modules-2479803-1.patch"
projects[features_override][version] = 2.0-rc3
projects[inline_conditions][version] = 1.0-alpha5
projects[libraries][version] = 2.2
projects[link][version] = 1.3
projects[metatag][version] = 1.7
projects[r4032login][version] = 1.8
projects[field_name_prefix_remove][version] = 1.2
projects[redirect][version] = 1.0-rc3
projects[rules][version] = 2.9
projects[strongarm][version] = 2.0
projects[taxonomy_machine_name][version] = 1.2
projects[token][version] = 1.6
projects[ultimate_cron][version] = 2.0-rc1
projects[uuid][version] = 1.0-beta1
projects[views][version] = 3.13
projects[views_bulk_operations][version] = 3.3
projects[xmlsitemap][version] = 2.2

; Performance
projects[entitycache][version] = 1.5

; Search related modules.
projects[search_api][version] = 1.16
projects[search_api_db][version] = 1.5
projects[search_api_sorts][version] = 1.5
projects[facetapi][version] = 1.5
projects[facetapi][patch][] = "https://drupal.org/files/1616518-term_remove_link-24.patch"
projects[facetapi][patch][2378693] = "https://www.drupal.org/files/issues/notice_undefined-2378693-3.patch"

; UI improvement modules.
projects[admin_menu][version] = 3.0-rc5
projects[admin_views][version] = 1.5
projects[adminimal_admin_menu] = 1.7
projects[image_delta_formatter][version] = 1.0-rc1
projects[jquery_update][version] = 2.7
projects[module_filter][version] = 2.0
projects[pathauto][version] = 1.3

; Internationalization.
projects[i18n][version] = "1.13"
projects[variable][version] = 2.5

; Base theme.
projects[adminimal_theme][version] = 1.23

