
ABOUT ACTUALYS PROFILE
======================



CONFIGURATION AND FEATURES
==========================

CRON
----

"cron ultimate" is enabled to be run by default all "15 minutes".
Don't forget to edit your crontab:

    */5 * * * * cd /path/to/project && /path/to/drush cron-run > /dev/null 2>&1 &


CONTRIBUTION
============

1) Update Drupal core:
(done from drupal root folder)

    drush make profiles/actualys/drupal-org-core.make .


2) Update contrib modules:
(done from drupal root folder)

    drush make --no-core --contrib-destination=. --concurrency=3 profiles/actualys/drupal-org.make profiles/actualys

